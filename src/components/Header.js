import React, { Component } from 'react';

import Logo from '../images/logo.png';
import LogoMobile from '../images/logo-mobile.png';
import Loup from '../images/loup.png';
import Place from '../images/place.png';
import User from '../images/user.png';
import Help from '../images/help.png';

class Header extends Component {
  constructor() {
    super()
    this.state = {
      showSecondaryMenu: true,
      searchBar: false,
      showSubmenu: false
    }

    this.removeSecondaryMenu = this.removeSecondaryMenu.bind(this)
    this.verifyScrollPosition = this.verifyScrollPosition.bind(this)
    this.openSearchbar = this.openSearchbar.bind(this)
    this.showSubmenu = this.showSubmenu.bind(this)
  }

  componentDidMount() {
    window.addEventListener('scroll', this.verifyScrollPosition);
  }

  verifyScrollPosition() {
    let windowSize = window.innerWidth;

    if (windowSize >= 1024) {
      this.removeSecondaryMenu(370);
    } else {
      this.removeSecondaryMenu(250);
    }
  }

  removeSecondaryMenu(size) {
    let scrollPosition = window.scrollY;

    if (scrollPosition >= size) {
      this.setState({ showSecondaryMenu: false })
    } else {
      this.setState({ showSecondaryMenu: true })
    }
  }

  openSearchbar() {
    let windowSize = window.innerWidth;

    if (windowSize <= 1280) {
      this.setState({ searchBar: !this.state.searchBar })
    }
  }

  showSubmenu(e) {
    e.preventDefault();

    this.setState({ showSubmenu: !this.state.showSubmenu })
  }

  render() {
    return (
      <header>
        <div className="main-header">
          <h1>
            <a href="#">
              <img className='main-header-logo-mobile' src={LogoMobile} alt="Logotipo Ingresso.com" />
              <img className='main-header-logo-desktop' src={Logo} alt="Logotipo Ingresso.com" />
            </a>
          </h1>

          <div className='main-header-searchbar'>
            <span className='main-header-searchbar-search'>
              <span className={this.state.searchBar ? 'active' : ''} >
                <input type="text" className={this.state.searchBar ? 'active' : ''} placeholder='Pesquise por filmes, cinemas...' />
              </span>
              <button onClick={() => this.openSearchbar()}>
                <img src={Loup} alt="Pesquisar" />
              </button>
            </span>

            <ul>
              <li><a href=""><span>Rio de Janeiro</span><img src={Place} alt="Local" /></a></li>
              <li>
                <a href="" onClick={(e) => this.showSubmenu(e)}>
                  <span>Olá, Frontend</span><img src={User} alt="Usuário" />
                </a>
                {
                  this.state.showSubmenu &&
                  <div className='submenu'>
                    <p>Olá! Espero que gostem do resultado :)</p>
                  </div>
                }
              </li>
              <li><a href=""><span>Atendimento</span> <img src={Help} alt="Ajuda" /></a></li>
            </ul>
          </div>
        </div>

        {
          this.state.showSecondaryMenu &&
          <div className="secondary-header">
            <ul>
              <li><a href="">Filmes</a></li>
              <li><a href="">Cinemas</a></li>
              <li><a href="">Eventos</a></li>
              <li><a href="">Rock In Rio</a></li>
            </ul>
          </div>
        }
      </header>

    )
  }
}

export default Header