import React, { Component } from 'react';
import Slider from "react-slick";
import axios from 'axios';

import MovieCard from './MovieCard';
import LoadIcon from '../images/load.svg';

class Movies extends Component {
  constructor() {
    super();

    this.state = {
      movies: [],
      loading: true,
      tagName: null,
      sliderSettings: {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true
      }
    }
  }

  componentDidMount() {
    axios.get('https://cors-anywhere.herokuapp.com/https://api-content.ingresso.com/v0/templates/highlights/1/partnership/home')
      .then(res => {
        this.setState({
          loading: false,
          movies: res.data
        })
      })

    this.windowSize();
  }

  verifyAge(rating) {
    console.log(rating)
    switch (rating) {
      case '10 anos':
        return 'years-10'
        break;
      case '12 anos':
        return 'years-12'
        break;
      case '14 anos':
        return 'years-14'
        break;
      case '16 anos':
        return 'years-16'
        break;
      case '18 anos':
        return 'years-18'
        break;
      case 'Verifique a Classificação':
        return 'verify-age'
        break;
      default:
        return 'free-years'
    }
  }

  windowSize() {
    if (window.innerWidth < 414) {
      this.setState({
        sliderSettings: {
          slidesToShow: 2,
          arrows: false
        }
      })
    } else if (window.innerWidth > 414 && window.innerWidth < 1080) {
      this.setState({
        sliderSettings: {
          slidesToShow: 3,
          arrows: false
        }
      })
    }
  }

  render() {
    return (
      <div className="movies">
        <div className="movies-label">
          <h3 className="movies-label-title">Em cartaz</h3>
          <a className="movies-label-link" href="#">Ver todos</a>
        </div>
        {
          this.state.loading &&
          <div className='movies-loading'>
            <img src={LoadIcon} alt="Carregando filmes" />
          </div>
        }
        <div className="movies-thumbnail">
          <ul className='movies-thumbnail-list'>
            <Slider {...this.state.sliderSettings}>
              {this.state.movies.map((movie, index) => (
                < li className='movies-thumbnail-list-item' key={index} >
                  <MovieCard
                    url={movie.event.trailers.length ? movie.event.trailers[0].url : movie.event.siteURL}
                    image={movie.event.images[0].url}
                    alt={movie.event.title}
                    movieTags={movie.event.tags}
                    age={movie.event.contentRating}
                    ageVerification={this.verifyAge(movie.event.contentRating)}
                  />
                </li>
              ))}

            </Slider>
          </ul>
        </div>
      </div >
    )
  }
}


export default Movies