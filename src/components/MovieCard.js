
import React from 'react';
import propTypes from 'prop-types';

const verifyTags = function (tag) {
  if (tag === 'Família') {
    return 'family'
  } else {
    return 'tranding'
  }
}

const MovieCard = ({ url, image, alt, movieTags, age, tagType, ageVerification }) => (
  <a href={url} target='_blank'>
    <img src={image} alt={alt} />
    <div className="movies-thumbnail-list-item-info">
      <div className="movies-thumbnail-list-item-info-tags">
        {movieTags.map((tag, index) => (
          <p key={index} className={'movies-thumbnail-list-item-info-tags ' + verifyTags(tag)}>{tag}</p>
        ))}
      </div>

      <div className='movies-thumbnail-list-item-info-name'>
        <p className={'movies-thumbnail-list-item-info-age ' + ageVerification}>
          {age}
        </p>
      </div>
    </div>
  </a>
)

MovieCard.propTypes = {
  url: propTypes.string.isRequired,
  image: propTypes.string.isRequired,
  alt: propTypes.string.isRequired,
  movieTags: propTypes.array.isRequired,
  age: propTypes.string.isRequired,
  tagType: propTypes.string,
  ageVerification: propTypes.string
}

export default MovieCard