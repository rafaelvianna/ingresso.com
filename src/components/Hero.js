import React from 'react';

const Hero = () => (
  <div className="hero">
    <div className='hero-text'>
      <div className="hero-text-tags">
        <p className='hero-text-tags-family'>Família</p>
        <p className='hero-text-tags-tranding'>Em Alta</p>
      </div>

      <h2 className="hero-text-title">Malévola - Dona do Mal</h2>

      <div className="hero-text-movie-type">
        <p>Aventura</p>
        <p>Fantasia</p>
      </div>
    </div>
  </div>
)

export default Hero
