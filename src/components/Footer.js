import React from 'react';

import Appstore from '../images/appstore.png';
import GooglePlay from '../images/googleplay.png';
import Facebook from '../images/facebook.png';
import Youtube from '../images/youtube.png';
import Instagram from '../images/instagram.png';
import Linkedin from '../images/linkedin.png';

const Footer = () => (
  <footer>
    <div className="footer-row-1">
      <div className="footer-row-1-app">
        <h5>Baixe nosso App</h5>
        <div>
          <a href="#"><img src={Appstore} alt="Baixar na apple Store" /></a>
          <a href="#"><img src={GooglePlay} alt="Baixar no google play" /></a>
        </div>
      </div>

      <div className="footer-row-1-network">
        <h5>Nos siga nas redes</h5>
        <ul>
          <li><a href="#"><img src={Facebook} alt="facebook" /></a></li>
          <li><a href="#"><img src={Youtube} alt="youtube" /></a></li>
          <li><a href="#"><img src={Instagram} alt="instagram" /></a></li>
          <li><a href="#"><img src={Linkedin} alt="linkedin" /></a></li>
        </ul>
      </div>

      <div className="footer-row-1-help">
        <h5>Precisa de ajuda?</h5>
        <a href="#" className='btn-primary'>Atendimento</a>
      </div>
    </div>

    <div className="footer-row-2">
      <div className="footer-row-2-menu">
        <h6>Menu</h6>
        <ul>
          <li><a href="">Filmes</a></li>
          <li><a href="">Cinemas</a></li>
          <li><a href="">Eventos</a></li>
        </ul>
      </div>

      <div className="footer-row-2-menu">
        <h6>Institucional</h6>
        <ul>
          <li><a href="">Quem Somos</a></li>
          <li><a href="">Assesoria de Imprensa</a></li>
          <li><a href="">Bootstrap ingresso.com</a></li>
          <li><a href="">Vale-Presente Corporativo</a></li>
          <li><a href="">Ingresso.com atende</a></li>
          <li><a href="">movieID.com</a></li>
        </ul>
      </div>

      <div className="footer-row-2-menu">
        <h6>Políticas</h6>
        <ul>
          <li><a href="">Privacidade e Segurança</a></li>
          <li><a href="">Meia-entrada</a></li>
          <li><a href="">Trocas e Cancelamentos</a></li>
          <li><a href="">Leis Estaduais e Municipais</a></li>
          <li><a href="">Termos de Uso</a></li>
        </ul>
      </div>

      <div className="footer-row-2-menu">
        <h6>Dúvidas</h6>
        <ul>
          <li><a href="">Atendimento</a></li>
          <li><a href="">Site ou emails falsos</a></li>
          <li><a href="">Procon RJ</a></li>
        </ul>
      </div>
    </div>

    <div className="footer-row-3">
      <small>Ingresso.com Ltda/CNPJ: 00.860.640/0001-71 <b>Endereço:</b> Rua da Quitanda, 86 - 9 andar - Centro - RJ - 20091-005</small>
      <small><span>Atendimento ao cliente</span> 2019 - Copyright Ingresso.com - Todos os direitos reservados</small>
    </div>
  </footer>
)

export default Footer