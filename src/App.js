import React from 'react';

import Header from './components/Header';
import Hero from './components/Hero';
import Movies from './components/Movies';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <Hero />
      <Movies />
      <Footer />
    </div>
  );
}

export default App;
