## Ingresso.com - processo seletivo

### Tecnologias utilizadas:
* React
* Sass

### Metodologias utilizadas:
* Mobile First
* BEM Css

### Como Rodar o projeto:
Clone este respositório, navegue até a pasta do projeto e rode os seguintes comandos:
* `npm install`
* `npm start`

O projeto estará rodando em [http://localhost:3000](http://localhost:3000)

### Observações do projeto
* Tomei a liberdade de modificar a disposição dos elementos nos cards dos filmes. Fiz isso para melhorar a leitura das informações dispostas.
* O carrossel mostra quantidades diferentes de filmes de acordo com o tamanho do dispositivo. Caso o teste seja realizado no browser, ao redimensionar sugiro fazer um reload para ter uma melhor experiência. 
* Todos os dados dos filmes estão dinâmicos vindos da API.
* A interação do menu acontece no link `Olá frontend` ( mobile e desktop ) e na lupa de busca ( no mobile apenas )

#### Porque o carrossel não se adapta sozinho?
A configuração da quantidade que será mostrada é de acordo com o tamanho do viewport. Porém o script é carregado na primeira renderização da página.

E isso em um projeto real pode ser prejudicial para a experiência do usuário?
R: Não! Porque o usuário final irá sempre fazer o carregamento de algum dispositivo com viewport fixo. E o script irá identificar e mostrar a quantidade correta para aquele tamanho.

